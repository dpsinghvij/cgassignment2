package cg.assignment.second.library.world;

import cg.assignment.second.library.models.Point;
import cg.assignment.second.library.models.Vector;

/**
 * Created by davinder on 18/10/16.
 * This class represents a ray object
 * A ray is essentially a line. It is also like a “vector” because it has a starting point and
 * points a certain direction
 * The parametric representation for line is PO+ VO*t
 */
public class Ray {

    private Vector V0;      // Ray vector
    private Point P0;       // Point of Origin of ray
    private float t=1;      // a variable to compute different lengths of Ray

    public Ray() {
    }

    public Ray(Vector v0, Point p0) {
        V0 = v0;
        P0 = p0;
    }

    public Vector getV0() {
        return V0;
    }

    public void setV0(Vector v0) {
        V0 = v0;
    }

    public Point getP0() {
        return P0;
    }

    public void setP0(Point p0) {
        P0 = p0;
    }

    public float getT() {
        return t;
    }

    public void setT(float t) {
        this.t = t;
    }

    /**
     * Get a Point for a particular value of t
     * @param t0 the value of t for which point is to be calculated
     * @return Point
     */
    public Point getPointWithT(double t0) {
        Vector vector=V0.multiply((float) t0);
        float x = P0.getX() + vector.getX();
        float y = P0.getY() + vector.getY();
        float z = P0.getZ() + vector.getZ();
        return new Point(x,y,z);
    }
}
