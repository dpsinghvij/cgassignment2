package cg.assignment.second.library.world;

import cg.assignment.second.library.models.Point;
import cg.assignment.second.library.models.Vector;

/**
 * Created by davinder on 18/10/16.
 * It represents a camera object and it position
 */
public class Camera {

    private Point pointVrp;         // Camera's Reference Point
    private Vector vectorVpn;       // Plane Normal Vector
    private Vector vectorVup;       // Camera's Up vector

    public Camera(Point pointVrp, Vector vectorVpn, Vector vectorVup) {
        this.pointVrp = pointVrp;
        this.vectorVpn = vectorVpn;
        this.vectorVup = vectorVup;
    }

    public Point getPointVrp() {
        return pointVrp;
    }

    public void setPointVrp(Point pointVrp) {
        this.pointVrp = pointVrp;
    }

    public Vector getVectorVpn() {
        return vectorVpn;
    }

    public void setVectorVpn(Vector vectorVpn) {
        this.vectorVpn = vectorVpn;
    }

    public Vector getVectorVup() {
        return vectorVup;
    }

    public void setVectorVup(Vector vectorVup) {
        this.vectorVup = vectorVup;
    }
}
