package cg.assignment.second.library.interfaces;

import cg.assignment.second.library.models.Point;
import cg.assignment.second.library.world.Ray;

/**
 * Created by davinder on 18/10/16.
 * An interface which is to be implemented by any object in the scene
 * which will need to be shown
 */
public interface PhysicalObject {
    Point checkIntersection(Ray ray);
    int shading(Ray ray,Point objectPoint, Point lightPosition);
}
