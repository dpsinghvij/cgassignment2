package cg.assignment.second.utils;

/**
 * Created by davinder on 17/10/16.
 */
public class Constants {
    public static final int NO_OF_COL = 512;
    public static final int NO_OF_ROWS = 512;
    public static final float XMIN = 0.0175f;
    public static final float XMAX = -0.0175f;
    public static final float YMIN = -0.0175f;
    public static final float YMAX = 0.0175f;
    public static final float FOCAL_LENGTH = .05f;
    public static final float INTENSITY_OF_POINT_SOURCE = 200;
}
