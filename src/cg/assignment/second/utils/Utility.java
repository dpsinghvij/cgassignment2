package cg.assignment.second.utils;


import cg.assignment.second.library.models.Point;
import cg.assignment.second.library.models.Vector;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Created by davinder on 3/10/16.
 */
public class Utility {

    public static void print(String message){
        System.out.println(message);
    }

    /**
     * Helper method to input values of vector elements
     * @param message name of vector
     * @return
     */
    public static Vector takeVectorAsInput(String message){
        Scanner scanner= new Scanner(System.in);
        Vector vector= new Vector();
        print("Please enter x value of vector "+message);
        vector.setX(scanner.nextFloat());
        print("Please enter y value of vector "+message);
        vector.setY(scanner.nextFloat());
        print("Please enter z value of vector "+message);
        vector.setZ(scanner.nextFloat());
        return vector;
    }


    /**
     * Helper method to input values of point elements
     * @param message name of Point
     * @return
     */
    public static Point takePointAsInput(String message){
        Scanner scanner= new Scanner(System.in);
        Point point= new Point();
        print("Please enter x value of point "+message);
        point.setX(scanner.nextFloat());
        print("Please enter y value of point "+message);
        point.setY(scanner.nextFloat());
        print("Please enter z value of point "+message);
        point.setZ(scanner.nextFloat());
        return point;
    }

    public static String fmt(float value) {
        return String.format("%6.3f",value);
    }

    /**
     * Reads object from a json file
     * For better code visibility and maintainability JSON has been used
     * @param t An object of type t to be returne
     * @param filename filename of the object
     * @return
     * @throws Exception An exception is thrown if file is not present
     */
    public static Object readFromFile(Class t,String filename) throws Exception {
        try {
            String basePath = "./src/objects/";
            JsonReader reader = new JsonReader(new FileReader(basePath+filename));
            // JSON object is converted to a Java Object
            Object o = new Gson().fromJson(reader, t);
            if(t.isInstance(o)){
                return o;
            }
            throw new Exception("Incorrect Object");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
