package cg.assignment.second;

import cg.assignment.second.library.models.Point;
import cg.assignment.second.library.models.Vector;
import cg.assignment.second.library.world.Camera;
import cg.assignment.second.library.world.Scene;
import cg.assignment.second.library.world.objects.Sphere;
import cg.assignment.second.ray.RayConstruction;
import com.google.gson.Gson;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Main extends JFrame {

    public static void main(String[] args) {

        Scene scene= new Scene();
        try {
            scene.renderFrame();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
